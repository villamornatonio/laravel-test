<?php
namespace App\Contracts;

interface EmailRepositoryInterface
{
    public function verify($request);
}
