<?php
namespace App\Repositories;

use App\Contracts\EmailRepositoryInterface;

class EmailRepository implements EmailRepositoryInterface
{
    public function verify($request)
    {
        $response = [];
        $email = $request->get('email');
        $emailArray = explode("@", $email);
        if (checkdnsrr(array_pop($emailArray), "MX")) {
            $response['message'] = trans('response.email_valid');
            $response['data'] = [];
        } else {
            $response['message'] = trans('response.email_not_valid');
            $response['data'] = $this->suggestEmailDomain($email);
        }
        return $response;
    }

    private function suggestEmailDomain($email)
    {
        $response = [];
        $emailArray = explode("@", $email);
        $domains = config('domains');
        $domain = substr($emailArray[1], 0,-3);
        $pattern = '/.*' . $domain  .'.*/i';
        $matches  = preg_grep ($pattern, $domains);
        $response = [
            'email' => $email,
            'suggestions' => $matches

        ];
        return $response;

    }

}
